import { Component } from '@angular/core';
import { NavController, LoadingController, ActionSheetController } from 'ionic-angular';
import { Http } from '@angular/http'; //Importa HTTP
import { InAppBrowser } from '@ionic-native/in-app-browser'; //Importa InAppBrowser
import 'rxjs/add/operator/map'; // Importar operador MAP

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public feeds: Array<string>;
  private url: string = "https://www.reddit.com/new.json";
  private newerPosts: string = "https://www.reddit.com/new.json?before=";
  private olderPosts: string = "https://www.reddit.com/new.json?after=";
  public noFilter: Array<any>;
  public hasFilter: boolean = false;

  constructor(public navCtrl: NavController,
    public http: Http,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController) {
    this.fetchContent();
  }

  fetchContent(): void {
    let loading = this.loadingCtrl.create({
      content: 'Buscando conteúdo...'
    });
    loading.present();
    this.http.get(this.url).map(res => res.json())
      .subscribe(data => {
        this.feeds = data.data.children;
        loading.dismiss();
        this.repairImages();
        this.removeFilters();
      });
  }

  itemSelected(feed: any): void {
    let browser = new InAppBrowser();
    browser.create(feed.data.url, '_system');
  }

  doInfinite(infiniteScroll) {

    let entries: any = this.feeds;

    let paramsUrl = (this.feeds.length > 0) ? entries[this.feeds.length - 1].data.name : "";
    
          this.http.get(this.olderPosts + paramsUrl).map(res => res.json())
            .subscribe(data => {
            
              this.feeds = this.feeds.concat(data.data.children);
              
              this.repairImages();
              infiniteScroll.complete();
              this.removeFilters();
            }); 
  }

  doRefresh(refresher) {
    
    let entries: any = this.feeds;
    let paramsUrl = entries[0].data.name;

    this.http.get(this.newerPosts + paramsUrl).map(res => res.json())
      .subscribe(data => {
      
        this.feeds = data.data.children.concat(this.feeds);
        this.repairImages();
        
        refresher.complete();
        this.removeFilters();
      });
  }
  
  repairImages(){
    this.feeds.forEach((e:any, i, a) => {
      if (!e.data.thumbnail || e.data.thumbnail.indexOf('b.thumbs.redditmedia.com') === -1 ) {  
        e.data.thumbnail = 'http://www.redditstatic.com/icon.png';
      }
    })
  }

  removeFilters(){
    this.noFilter = this.feeds;
    this.hasFilter = false;
  }

  showFilters() :void {
    
        let actionSheet = this.actionSheetCtrl.create({
          title: 'Filter options:',
          buttons: [
            {
              text: 'Música',
              handler: () => {
                this.feeds = this.noFilter.filter((item) => item.data.subreddit.toLowerCase() === "music");
                this.hasFilter = true;
              }
            },
            {
              text: 'Filmes',
              handler: () => {
                this.feeds = this.noFilter.filter((item) => item.data.subreddit.toLowerCase() === "movies");
                this.hasFilter = true;
              }
            },        
            {
              text: 'Nenhum filtro',
              role: 'cancel',
              handler: () => {
                this.feeds = this.noFilter;
                this.hasFilter = false;
              }
            }
          ]
        });
    
        actionSheet.present();
    
      }

}
