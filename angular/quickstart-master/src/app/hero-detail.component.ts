import { Component, Input } from '@angular/core';
import { Heroi } from './heroi';

@Component({
    selector: 'hero-detail',
    templateUrl: `./hero-detail.component.html`,
})

export class HeroDetailComponent {
    @Input() heroi: Heroi;
}