import { Component } from '@angular/core';
import { Heroi } from './heroi';

const HEROIS: Heroi[] = [
  { id: 1, nome: 'Blast', classe: 'S' },
  { id: 2, nome: 'Tornado of Terror', classe: 'S' },
  { id: 3, nome: 'Silver Fang', classe: 'S' },
  { id: 4, nome: 'Atomic Samurai', classe: 'S' },
  { id: 5, nome: 'Doutei', classe: 'S' },
  { id: 6, nome: 'Metal Knight', classe: 'S' },
  { id: 7, nome: 'King', classe: 'S' },
  { id: 8, nome: 'Zombieman', classe: 'S' },
  { id: 9, nome: 'Kudou Kishi', classe: 'S' },
  { id: 10, nome: 'Butagami', classe: 'S' },
  { id: 11, nome: 'Superalloy Darkshine', classe: 'S' },
  { id: 12, nome: 'Banken-Man', classe: 'S' },
  { id: 13, nome: 'Senkou no Flash', classe: 'S' },
  { id: 14, nome: 'Genos', classe: 'S' },
  { id: 15, nome: 'Tanktop Master', classe: 'S' },
  { id: 16, nome: 'Metal Bat', classe: 'S' },
  { id: 17, nome: 'Puri-Puri-Prisoner', classe: 'S' },
];

@Component({
  selector: 'my-app',
  templateUrl: `./app.component.html`,
  styleUrls: [`./app.component.css`]
})
export class AppComponent {
  titulo = 'Associação de Heróis';
  herois = HEROIS;
  selectedHeroi: Heroi;

  onSelect(heroi: Heroi) {
    this.selectedHeroi = heroi;
  }
}
