import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lista:string[];

  constructor(public navCtrl: NavController) {
    this.lista = ["アルファ", "ブラヴォ"];
  }

}
